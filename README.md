Codefish
======================

This application is used for screencast tutorials about how to set up Codeship projects with Django applications. It is based on [Google's app engine django template](https://code.google.com/p/google-app-engine-samples/source/browse/trunk/django_example).

![Codeship Status](https://www.codeship.io/projects/ccd212a0-64aa-0131-452c-7a6d2bba8338/status)

commitdsa